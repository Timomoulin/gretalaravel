<?php

namespace Database\Seeders;

use App\Models\Destination;
use App\Models\Pays;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $pays1=Pays::create(["nom"=>"france","population"=>"70000000","region"=>"europe"]);
        $pays2=Pays::create(["nom"=>"egypt","population"=>"102334403","region"=>"afrique"]);

        $destination1=Destination::create([
            "nom"=>"Sejour a Paris",
            "description"=>"Un sejour dans la ville des lumières",
            "dateDebut"=>"2022-01-01",
            "prix"=>300.2,
            "estDisponible"=>true,
            "duree"=>7,
            "pays_id"=>$pays1->id
        ]);

        $admin=Role::create(["nom"=>"admin"]);
        $client=Role::create(["nom"=>"client"]);

        $moi=User::create([
            "name"=>"moulin",
            "firstname"=>"timothee",
            "email"=>"timomoulin@msn.com",

            "password"=>bcrypt("timomoulin@msn.com1"),
            "roles_id"=>$admin->id
        ]);

    }
}
