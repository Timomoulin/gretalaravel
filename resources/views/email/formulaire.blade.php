@extends("template")
@section("titre")
Formulaire de contacte
@endsection
@section("contenu")

 <div class="container">
     <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
         <form action="/contact" method="post">
             @csrf
             <div class="row mb-2">
                 <label for="email">Email</label>
                 <input name="email" required type="email" class="form-control" id="email" value="{{old("email")}}" placeholder="Enter Email">
                 @error("email")
                 <div class="text-danger">{{$message}}</div>
                 @enderror
                </div>

             <div class="row mb-2">
                 <label for="sujet">Sujet</label>
                 <input name="sujet" required type="text" class="form-control" id="sujet" value="{{old("sujet")}}" placeholder="Enter Sujet">
                 @error("sujet")
                 <div class="text-danger">{{$message}}</div>
                 @enderror
                </div>

             <div class="row mb-2">
                 <label for="message"></label>
                 <textarea name="message" required type="text" class="form-control" id="message" value="{{old("message")}}" placeholder="Enter Message "> </textarea>
                 @error("message")
                 <div class="text-danger">{{$message}}</div>
                 @enderror
                </div>
         <button type="submit" class="btn btn-primary">Envoyer</button>
         </form>
     </div>
 </div>
@endsection
