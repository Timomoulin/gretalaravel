<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OnlyAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        //Si l'utilisateur n'est pas conneceter
        $utilisateur=auth()->user();
        if($utilisateur==null)
        {
            abort(403);
        }
        //Si le nom du role de l'utilisateur n'est pas admin
        elseif($utilisateur->role->nom!="admin"){
            abort(403);
        }
        return $next($request);
    }
}
