<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function contactForm(){
        return view("email.formulaire");
    }

    public function envoyerEmail(Request $request){
        $data=$request->validate([
            "email"=>"required|email",
            "sujet"=>"required",
            "message"=>"required"
        ]);

        Mail::to('timotheemoulin01@gmail.com')->send(new Contact($data));

        // Mail::send('email.email', [
        //     "email"=>$data["email"],
        //     "sujet"=>$data["sujet"],
        //     "texte"=>$data["message"]
        // ], function ($message) use ($data)  {
        //     $message->from($data["email"]);
        //     $message->to('timotheemoulin01@gmail.com', 'Admin');
        //     $message->replyTo($data["email"],);
        //     $message->subject('GretaVoyages '.$data["sujet"]);
        //     $message->priority(3);

        // });
    }
}
